import { generateBracket } from "./helpers/generateBracket.js";
import { generateBracketLayout } from "./helpers/generateBracketLayout.js";

let roundsData = []; // create global rounds data array

// get name-input textarea element
let nameInput = document.getElementById("name-input");

// add a default value to name-input
let defaultPlayers = [...Array(20).keys()].map((n) => `Player ${n + 1}`);
nameInput.value = defaultPlayers.join("\n");

// generate playerList from value of textarea
const handleButtonClick = (e) => {
  e.preventDefault();
  let nameInputValue = nameInput.value;
  let playerList = [...new Set(nameInputValue.split("\n"))]; // removes duplicate entries
  roundsData = generateBracket(playerList);
  generateBracketLayout(roundsData);
};

// get the create-bracket-button element
let createBracketButton = document.getElementById("create-bracket-button");

createBracketButton.addEventListener("click", handleButtonClick);
