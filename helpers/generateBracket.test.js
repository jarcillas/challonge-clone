// const { generateFirstRoundSeeding, getRoundCount } = require("./namesInput");

import { expect } from "@esm-bundle/chai";

import {
  getRoundCount,
  generateFirstRoundSeeding,
  generateSecondRound,
  generateOtherRounds,
  generateBracket,
} from "./generateBracket";

describe("generateFirstRoundSeeding works correctly", () => {
  it("throws an error if roundCount is a string", () => {
    expect(() => generateFirstRoundSeeding("2")).to.throw(
      "roundCount should be a number."
    );
  });

  it("throws an error if roundCount is null", () => {
    expect(() => generateFirstRoundSeeding(null)).to.throw(
      "roundCount should be a number."
    );
  });

  it("throws an error if roundCount is a boolean", () => {
    expect(() => generateFirstRoundSeeding(true)).to.throw(
      "roundCount should be a number."
    );

    expect(() => generateFirstRoundSeeding(false)).to.throw(
      "roundCount should be a number."
    );
  });

  it("throws an error for 0 rounds", () => {
    expect(() => generateFirstRoundSeeding(0)).to.throw(
      "roundCount should be at least 1."
    );
  });

  it("throws an error for -1 rounds", () => {
    expect(() => generateFirstRoundSeeding(-1)).to.throw(
      "roundCount should be at least 1."
    );
  });

  it("generates [1, 2] for 1 round", () => {
    expect(generateFirstRoundSeeding(1)).to.eql([1, 2]);
  });

  it("generates [1, 4, 2, 3] for 2 round", () => {
    expect(generateFirstRoundSeeding(2)).to.eql([1, 4, 2, 3]);
  });

  it("generates [1, 8, 4, 5, 2, 7, 3, 6] for 2 round", () => {
    expect(generateFirstRoundSeeding(3)).to.eql([1, 8, 4, 5, 2, 7, 3, 6]);
  });
});

describe("getRoundCount works correctly", () => {
  it("throws an error if playerCount is 0", () => {
    expect(() => getRoundCount(0)).to.throw(
      "playerCount should be at least 2."
    );
  });

  it("gets 1 round if playerCount is 2", () => {
    expect(getRoundCount(2)).to.equal(1);
  });

  it("gets 2 rounds if playerCount is 3", () => {
    expect(getRoundCount(3)).to.equal(2);
  });

  it("gets 2 rounds if playerCount is 4", () => {
    expect(getRoundCount(4)).to.equal(2);
  });

  it("gets 3 rounds if playerCount is 5", () => {
    expect(getRoundCount(5)).to.equal(3);
  });

  it("gets 3 rounds if playerCount is 8", () => {
    expect(getRoundCount(8)).to.equal(3);
  });

  it("gets 4 rounds if playerCount is 9", () => {
    expect(getRoundCount(9)).to.equal(4);
  });
});

describe("generateSecondRound works correctly", () => {
  it("gives [null, null] if round1 has no byes", () => {
    const round1 = ["a", "b", "c", "d"];
    expect(generateSecondRound(round1)).to.eql([null, null]);
  });

  it("gives ['a', null] if round1 with length 4 has a Bye", () => {
    const round1 = ["a", "Bye", "c", "d"];
    expect(generateSecondRound(round1)).to.eql(["a", null]);
  });

  it("gives ['a', 'c'] if round1 with length 4 has two Byes", () => {
    const round1 = ["a", "Bye", "c", "Bye"];
    expect(generateSecondRound(round1)).to.eql(["a", "c"]);
  });

  it("gives [null, null, null, null] if round1 with length 8 has no Byes", () => {
    const round1 = ["a", "b", "c", "d", "e", "f", "g", "h"];
    expect(generateSecondRound(round1)).to.eql([null, null, null, null]);
  });

  it("gives ['a', null, null, null] if round1 with length 8 has 1 Bye", () => {
    const round1 = ["a", "Bye", "c", "d", "e", "f", "g", "h"];
    expect(generateSecondRound(round1)).to.eql(["a", null, null, null]);
  });

  it("gives ['a', null, 'e', null] if round1 with length 8 has 2 Byes", () => {
    const round1 = ["a", "Bye", "c", "d", "e", "Bye", "g", "h"];
    expect(generateSecondRound(round1)).to.eql(["a", null, "e", null]);
  });

  it("gives ['a', 'c', 'e', null] if round1 with length 8 has 2 Byes", () => {
    const round1 = ["a", "Bye", "c", "Bye", "e", "Bye", "g", "h"];
    expect(generateSecondRound(round1)).to.eql(["a", "c", "e", null]);
  });
});

describe("generateOtherRounds works correctly", () => {
  it("gives [] if roundCount = 2", () => {
    expect(generateOtherRounds(2)).to.eql([]);
  });

  it("gives [[null, null]] if roundCount = 3", () => {
    expect(generateOtherRounds(3)).to.eql([[null, null]]);
  });

  it("gives [[null, null, null, null], [null, null]] if roundCount = 4", () => {
    expect(generateOtherRounds(4)).to.eql([
      [null, null, null, null],
      [null, null],
    ]);
  });

  it("gives [Array(8).fill(null), Array(4).fill(null), Array(2).fill(null)] if roundCount = 5", () => {
    expect(generateOtherRounds(5)).to.eql([
      Array(8).fill(null),
      Array(4).fill(null),
      Array(2).fill(null),
    ]);
  });
});

describe("generateBracket works correctly", () => {
  it("gives [['a', 'b']] if playerList is ['a', 'b']", () => {
    expect(generateBracket(["a", "b"])).to.eql([["a", "b"]]);
  });

  it("gives [['a', 'Bye', 'b', 'c'], ['a', null]] if playerList is ['a', 'b', 'c']", () => {
    const playerList = ["a", "b", "c"];

    expect(generateBracket(playerList)).to.eql([
      ["a", "Bye", "b", "c"],
      ["a", null],
    ]);
  });

  it("gives [['a', 'd', 'b', 'c'], [null, null]] if playerList is ['a', 'b', 'c', 'd']", () => {
    const playerList = ["a", "b", "c", "d"];

    expect(generateBracket(playerList)).to.eql([
      ["a", "d", "b", "c"],
      [null, null],
    ]);
  });

  it("gives correct bracket if playerList is ['a', 'b', 'c', 'd', 'e']", () => {
    const playerList = ["a", "b", "c", "d", "e"];

    expect(generateBracket(playerList)).to.eql([
      ["a", "Bye", "d", "e", "b", "Bye", "c", "Bye"],
      ["a", null, "b", "c"],
      [null, null],
    ]);
  });

  it("gives correct bracket if playerList is ['a', 'b', 'c', 'd', 'e', 'f']", () => {
    const playerList = ["a", "b", "c", "d", "e", "f"];

    expect(generateBracket(playerList)).to.eql([
      ["a", "Bye", "d", "e", "b", "Bye", "c", "f"],
      ["a", null, "b", null],
      [null, null],
    ]);
  });
});
