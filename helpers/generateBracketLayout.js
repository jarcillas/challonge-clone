export { generateBracketLayout, handlePlayerClick };

const generateBracketLayout = (roundsData) => {
  let roundCount = roundsData.length;

  // get the #bracket-container element
  let bracketsContainer = document.getElementById("brackets-container");

  for (let roundNum = 0; roundNum < roundCount; roundNum++) {
    const roundDiv = document.createElement("div");
    roundDiv.className = "round";
    roundDiv.id = `r${roundNum}`;
    bracketsContainer.append(roundDiv);

    for (
      let matchNum = 0;
      matchNum < roundsData[roundNum].length / 2;
      matchNum++
    ) {
      const matchDiv = document.createElement("div");
      const playerUpDiv = document.createElement("div");
      const playerDownDiv = document.createElement("div");

      matchDiv.className = "match";

      playerUpDiv.className = "up player";
      playerDownDiv.className = "down player";
      playerUpDiv.id = `r${roundNum}p${matchNum * 2}`;
      playerDownDiv.id = `r${roundNum}p${matchNum * 2 + 1}`;
      playerUpDiv.roundNum = roundNum;
      playerDownDiv.roundNum = roundNum;
      playerUpDiv.playerPos = matchNum * 2;
      playerDownDiv.playerPos = matchNum * 2 + 1;

      playerUpDiv.nextDivId = `r${roundNum + 1}p${matchNum}`;
      playerDownDiv.nextDivId = `r${roundNum + 1}p${matchNum}`;

      playerUpDiv.innerHTML = `<div>${
        roundsData[roundNum][matchNum * 2] ?? ""
      }</div>`;

      playerDownDiv.innerHTML = `<div>${
        roundsData[roundNum][matchNum * 2 + 1] ?? ""
      }</div>`;

      playerUpDiv.addEventListener("click", (e) =>
        handlePlayerClick(e, roundsData)
      );
      playerDownDiv.addEventListener("click", (e) =>
        handlePlayerClick(e, roundsData)
      );

      playerUpDiv.addEventListener("mouseenter", handlePlayerMouseEnter);
      playerDownDiv.addEventListener("mouseenter", handlePlayerMouseEnter);

      playerUpDiv.addEventListener("mouseleave", handlePlayerMouseLeave);
      playerDownDiv.addEventListener("mouseleave", handlePlayerMouseLeave);

      roundDiv.append(matchDiv);
      matchDiv.append(playerUpDiv, playerDownDiv);
    }
  }
};

const handlePlayerClick = (event, roundsData) => {
  let playerDiv;
  if (event.target.classList.contains("player")) {
    playerDiv = event.target;
  } else {
    playerDiv = event.target.parentNode;
  }

  const roundNum = playerDiv.roundNum;

  const playerPos = playerDiv.playerPos;
  const playerName = roundsData[roundNum][playerPos];

  // function that clears
  const clearRest = (playerDiv) => {
    if (!playerDiv) {
      return;
    }

    playerDiv.children[0].innerText = "";
    roundsData[playerDiv.roundNum][playerDiv.playerPos] = null;
    clearRest(document.getElementById(playerDiv.nextDivId));
  };

  const targetPlayer = document.getElementById(playerDiv.nextDivId);
  if (targetPlayer) {
    const targetPlayerPos = targetPlayer.playerPos;
    if (
      targetPlayer.children[0].innerText !== "" &&
      playerName !== targetPlayer.children[0].innerText &&
      playerName !== "Bye"
    ) {
      clearRest(document.getElementById(targetPlayer.nextDivId));
    }

    if (playerName !== "Bye") {
      targetPlayer.classList.add("glow");
      targetPlayer.children[0].innerText = playerName;
      roundsData[roundNum + 1][targetPlayerPos] = playerName;
    }
  }
};

// create mouseenter handler
const handlePlayerMouseEnter = (event) => {
  const playerName = event.target.children[0].innerText;
  if (playerName === "Bye" || playerName === "") {
    return;
  }
  const playerDivList = document.getElementsByClassName("player");
  for (let playerDiv of playerDivList) {
    if (playerDiv.children[0].innerText === playerName) {
      playerDiv.classList.add("glow");
    }
  }
};

// create mouseleave handler
const handlePlayerMouseLeave = (event) => {
  const playerName = event.target.children[0].innerText;
  if (playerName === "Bye" || playerName === "") {
    return;
  }
  const playerDivList = document.getElementsByClassName("player");
  for (let playerDiv of playerDivList) {
    playerDiv.classList.remove("glow");
  }
};
