export {
  generateBracket,
  getRoundCount,
  generateFirstRoundSeeding,
  generateSecondRound,
  generateOtherRounds
};

const generateBracket = (playerList) => {
  let roundsData = []; // empty array to contain roundsData

  // find the number of tournament rounds
  const roundCount = getRoundCount(playerList.length);

  roundsData.push(generateFirstRound(playerList));

  if (roundCount > 1) {
    roundsData.push(generateSecondRound(roundsData[0]));
  }

  if (roundCount > 2) {
    roundsData = [...roundsData, ...generateOtherRounds(roundCount)];
  }

  return roundsData;
};

// a recursive function that generates the 1st round based on number of rounds
const generateFirstRoundSeeding = (roundCount) => {
  if (typeof roundCount !== "number") {
    throw new Error("roundCount should be a number.");
  }

  if (roundCount < 0) {
    throw new Error("roundCount should be at least 1.");
  }

  if (roundCount === 1) {
    return [1, 2];
  } else {
    let tempArray = [];
    for (let num of generateFirstRoundSeeding(roundCount - 1)) {
      tempArray.push(num);
      tempArray.push(2 ** roundCount - num + 1);
    }
    return tempArray;
  }
};

// function to get the number of rounds needed based on playerCount
const getRoundCount = (playerCount) => {
  if (playerCount < 2) {
    throw new Error("playerCount should be at least 2.");
  }
  return Math.ceil(Math.log2(playerCount));
};

const generateFirstRound = (playerList) => {
  // find the number of players
  const playerCount = playerList.length;

  // find the number of tournament rounds
  const roundCount = getRoundCount(playerCount);

  // find the number of byes
  const byeCount = 2 ** roundCount - playerCount;

  // create a playerList that includes byes
  const playerByeList = [...playerList, ...Array(byeCount).fill("Bye")];

  return generateFirstRoundSeeding(roundCount).map(
    (num) => playerByeList[num - 1]
  );
};

const generateSecondRound = (round1) => {
  let round2 = [];
  for (let k = 0; k < round1.length / 2; k++) {
    if (round1[2 * k + 1] === "Bye") {
      round2.push(round1[2 * k]);
    } else {
      round2.push(null);
    }
  }
  return round2;
};

const generateOtherRounds = (roundCount) => {
  let otherRounds = [];

  for (let j = 2; j < roundCount; j++) {
    let roundArray = Array(2 ** (roundCount - j)).fill(null);
    otherRounds.push(roundArray);
  }

  return otherRounds;
};
