import { expect } from "@esm-bundle/chai";
import { resetMouse, sendMouse } from "@web/test-runner-commands";

import { generateBracketLayout } from "./generateBracketLayout";

describe("generateBracketLayout works correctly", () => {
  let parentDiv;

  beforeEach(() => {
    parentDiv = document.createElement("div");
    parentDiv.id = "brackets-container";
    document.body.append(parentDiv);
  });

  afterEach(() => {
    parentDiv.remove();
  });

  it("appends 1 round div to the bracket-container div if roundsData has length 1", () => {
    generateBracketLayout([["A", "B"]]);

    expect(parentDiv.children.length).to.equal(1);
    parentDiv.children.forEach((childDiv) => {
      expect(childDiv.classList.contains("round")).to.equal(true);
    });
  });

  it("appends 2 round divs to the bracket-container div if roundsData has length 2", () => {
    generateBracketLayout([
      ["A", "B", "C", "D"],
      [null, null],
    ]);

    expect(parentDiv.children.length).to.equal(2);
    parentDiv.children.forEach((childDiv) => {
      expect(childDiv.classList.contains("round")).to.equal(true);
    });
  });
});

describe("handlePlayerClick works correctly", () => {
  let parentDiv;
  let clickedPlayerDiv;
  let targetPlayerDiv;

  function getMiddleOfElement(element) {
    const { x, y, width, height } = element.getBoundingClientRect();

    return {
      x: Math.floor(x + window.pageXOffset + width / 2),
      y: Math.floor(y + window.pageYOffset + height / 2),
    };
  }

  beforeEach(() => {
    parentDiv = document.createElement("div");
    parentDiv.id = "brackets-container";
    document.body.append(parentDiv);
    generateBracketLayout([
      ["Player 1", "Player 2", "Player 3", "Player 4"],
      [null, null],
    ]);
    clickedPlayerDiv = parentDiv.children[0].children[0].children[0];
    targetPlayerDiv = document.getElementById(clickedPlayerDiv.nextDivId);
  });

  afterEach(async () => {
    parentDiv.remove();
    await resetMouse();
  });

  it("adds player to next round", async () => {
    const { x, y } = getMiddleOfElement(clickedPlayerDiv);

    await sendMouse({ type: "click", position: [x, y] });
    expect(targetPlayerDiv.innerText).to.equal(clickedPlayerDiv.innerText);
  });

  it("adds glow class to next round when clicking", async () => {
    const { x, y } = getMiddleOfElement(clickedPlayerDiv);

    await sendMouse({ type: "click", position: [x, y] });
    expect(targetPlayerDiv.classList.contains("glow")).to.equal(true);
  });

  it("removes glow class when mouse leaves player div", async () => {
    const { x, y } = getMiddleOfElement(clickedPlayerDiv);

    await sendMouse({ type: "click", position: [x, y] });
    expect(targetPlayerDiv.classList.contains("glow")).to.equal(true);
    await resetMouse();
    expect(targetPlayerDiv.classList.contains("glow")).to.equal(false);
  });

  it("adds glow class back when mouse leaves then re-enters player div", async () => {
    const { x, y } = getMiddleOfElement(clickedPlayerDiv);

    await sendMouse({ type: "click", position: [x, y] });
    expect(targetPlayerDiv.classList.contains("glow")).to.equal(true);
    await resetMouse();
    expect(targetPlayerDiv.classList.contains("glow")).to.equal(false);
    await sendMouse({ type: "move", position: [x, y] });
    expect(targetPlayerDiv.classList.contains("glow")).to.equal(true);
  });
});
